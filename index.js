const express = require('express');
const mongoose = require('mongoose');
const app = express();

const port = 3001;

app.use(express.json())
app.use(express.urlencoded({extended:true}))


//Get connection string and change the password
// mongodb+srv://seanmarkmira:qwerty12345@zuitt-bootcamp.1noa4.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

//Step 16 change myFirstDatabase to s30. MongoDB will automatically create the db for us
// mongodb+srv://seanmarkmira:qwerty12345@zuitt-bootcamp.1noa4.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

//Step 17 connecting to MongoDB Atlas -add connect() method
mongoose.connect('mongodb+srv://seanmarkmira:qwerty12345@zuitt-bootcamp.1noa4.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',{
	useNewUrlParser:true,
	useUnifiedTopology:true
});

//Step 18a add the connection string as the 1st arguement
//Step18b add the this object to allow connection
/*
	{
		addNewUrlParser:true,
		useUnifiedTopology:true
	}
	*/

//Step 19 Set notification for connection success or failure by using connection property of mongoose
//mongoose.connection;

//Step 20 store it in a variable called db
let db = mongoose.connection;

//step21
//print errors in the browser console and in the terminal
db.on("error", console.error.bind(console,"connection error"));

//step 22 if the connection is succesful, out this in the console
db.once("open", ()=> console.log("We are connected to the cloud DB"))

//step 23 Schemas determine the structure of the documents to be written in the database

const taskSchema = new mongoose.Schema({
	//Define the fields with the corresponding data type
	//For a task, it needs a "task name" and "task status"
	//There is a field called "name" and its data type is "String"
	name: String,
	//There is a field called "status" that is a "string" and the default value is "pending"
	status: {
		type: String,
		default:"pending"
	}
})


//Models use schemas and they act as the middleman from the server (JS code) to our database
//Create a model
//Models must be in singular form and capitalized
//The first parameter of the Mongoose model method indicates the collection in where to store the data

//The second parameter is used to specify the Schema/blueprint
//of the documents that will be stored in the MongoDB collection

//using mongoose, the package was programmed well enought that it automatically converts the singular form of the model name into a plural form when creating a collection in postman
const Task = mongoose.model("Task",taskSchema)

//Step 25 Create route to add task
//Use the Task model to interact with the tasks collection
app.post('/tasks', (req, res)=>{
	//if you found something as a result of our object against the collection, used it in the function callback that will be defined
	Task.findOne({name: req.body.name},(err, result)=>{
		if(result !=null && result.name == req.body.name){
			return res.send('Duplicate task found')
		}else{
			let newTask = new Task({
				name:req.body.name
			})
			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send('The data is already been sent')
				}
			})
		}
	})
})

// Step 27 Get all tasks
// See all the contents of the task collection via the Task model
//"find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
app.get('/tasks',(req,res)=>{
	Task.find({}, (err,result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json(result)
		}
	})
})

//Activity
//1. Create a User schema

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

//2. Make a model
const User = mongoose.model("User",userSchema)

//3. Register a User
/*
1. add a functionality to check if there are duplicate users
- if the user already exists in the database, we return an error
- if the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Craete a new User object with a 'username' and 'password' fields/properties
*/

app.post('/signup',(req,res)=>{
	User.findOne({username: req.body.username},(err, result)=>{
		if(result !=null && result.username == req.body.username){
			return res.send('There is a registered username. Enter a unique one.')
		}else{
			let newUser = new User({
				username:req.body.username,
				password:req.body.password
			})
			newUser.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).json({success: true})
				}
			})
		}
	})
})

app.get('/signup',(req,res)=>{
	User.find({}, (err,result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json(result)
		}
	})
})

app.delete('/signup',(req,res)=>{
	User.deleteMany({}, (err,result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).send('deleted all entries')
		}
	})
})

app.listen(port,()=>{
	console.log('listening to port 3001')
})
